//Soal No. 1 (Array to Object)
console.log(""); 
console.log("Soal No. 1 (Array to Object)");
console.log("Jawaban : ");

function arrayToObject(arr) {
    if(arr==undefined ||arr.length==0){
        console.log("Error case : Parrameter must be asign with array");
    }else{
        var data ={};
        
        var now = new Date()
        var thisYear = now.getFullYear() // 2020 (tahun sekarang)
        for(var i=0; i<arr.length;i++){ 
            var tempData = {};   
            tempData.firstName=arr[i][0];
            tempData.lastName=arr[i][1];
            tempData.gender=arr[i][2];
            tempData.age=arr[i][3]!=undefined && arr[i][3]<thisYear ? thisYear-arr[i][3] : "Invalid birth year";
            data[(i+1)+". "+tempData.firstName+" "+ tempData.lastName]=tempData;
        }
        
        console.log(data);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


//Soal No. 2 (Shopping Time)
console.log(""); 
console.log("Soal No. 2 (Shopping Time)");
console.log("Jawaban : ");

var sale={
    1: { brandName: 'Sepatu Stacattu', price:1500000  },
    2: { brandName: 'Baju Zoro', price: 500000 },
    3: { brandName: 'Baju H&N', price: 250000 },
    4: { brandName: 'Sweater Uniklooh', price: 175000 },
    5: { brandName: 'Casing Handphone', price: 50000 }
  };



function shoppingTime(memberId, money) {
    var result;
    if((memberId!=undefined && memberId!="") && money!=undefined){
        var list=[];
        var changeMoney=money;
        var index=1;
        while (sale.hasOwnProperty(index)) {
            if(changeMoney>=sale[index].price){
            list.push(sale[index].brandName);
            changeMoney=changeMoney-sale[index].price;
            }
            index++;
        }
        var  data={
            memberId:memberId,
            money: money,
            listPurchased:list,
            changeMoney: changeMoney 
          };     
        result=list.length>0?data:"Mohon maaf, uang tidak cukup";
    } else{
        result="Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    return result;
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


//Soal No. 3 (Naik Angkot)
console.log(""); 
console.log("Soal No. 3 (Naik Angkot)");
console.log("Jawaban : ");

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var result=[];
    for(var i=0; i<arrPenumpang.length;i++){ 
        var tempData = {};   
        tempData.penumpang=arrPenumpang[i][0];
        tempData.naikDari=arrPenumpang[i][1];
        tempData.tujuan=arrPenumpang[i][2];
        tempData.bayar=(rute.indexOf(arrPenumpang[i][2])-rute.indexOf(arrPenumpang[i][1]))*2000;
        result.push(tempData);
    }
    return result;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]