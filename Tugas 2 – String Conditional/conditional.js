//Tugas Conditional If-else

console.log("Tugas Conditional If-else")

var nama = "John"
var peran = ""

console.log("Jawaban : ");

if (nama==""&&peran=="" ) {
    // Output untuk Input nama = '' dan peran = ''
    console.log("Nama harus diisi!");
} else if(nama==""){
    console.log("Nama harus diisi!");
}
else if(peran==""){
    //Output untuk Input nama = 'John' dan peran = ''
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
} else if(peran=="Penyihir"){
    //Output untuk Input nama = 'Jane' dan peran 'Penyihir'
    console.log("Selamat datang di Dunia Werewolf,  "+nama);
    console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
}else if(peran=="Guard"){
    //Output untuk Input nama = 'Jenita' dan peran 'Guard'
    console.log("Selamat datang di Dunia Werewolf,  "+nama);
    console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if(peran=="Werewolf"){
    //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
    console.log("Selamat datang di Dunia Werewolf,  "+nama);
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Peran tidak ditemukan!");
}
console.log("")

//Tugas Conditional Switch Case

console.log("Tugas Conditional Switch Case")

var tanggal= 21;  // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun= 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch(bulan) {
    case 1:   { bulan='Januari'; break; }
    case 2:   { bulan='Pebruari'; break; }
    case 3:   { bulan='Maret'; break; }
    case 4:   { bulan='April'; break; }
    case 5:   { bulan='Mei'; break; }
    case 6:   { bulan='Juni'; break; }
    case 7:   { bulan='Juli'; break; }
    case 8:   { bulan='Agustus'; break; }
    case 9:   { bulan='September'; break; }
    case 10:   { bulan='Oktober'; break; }
    case 11:   { bulan='November'; break; }
    case 12:   { bulan='Desember'; break; }
    default:  { break; }
}

console.log("Jawaban : ");

console.log(tanggal+" "+bulan+" "+tahun);