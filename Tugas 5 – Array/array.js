//Soal No. 1 (Range)
console.log(""); 
console.log("Soal No. 1 (Range)");
console.log("Jawaban : ");

function range(startNum,finishNum) {
    var angka=[];
    var index=0;
    var result;
   if(startNum<finishNum){
        for(startNum; startNum <= finishNum; startNum++) {
            angka[index]=startNum;
            index++;
            }
            result=angka;
    }else if(startNum>finishNum){
        for(startNum; startNum >= finishNum; startNum--) {
            angka[index]=startNum;
            index++;
            }
            result=angka;
    }else if(startNum==undefined || finishNum==undefined){
        result= -1;
    }
   
    return result;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


//Soal No. 2 (Range with Step)
console.log(""); 
console.log("Soal No. 2 (Range with Step)");
console.log("Jawaban : "); 
  
function rangeWithStep(startNum,finishNum,step=1) {
    var angka=[];
    var index=0;
    var result;
   if(startNum<finishNum){
        for(startNum; startNum <= finishNum; startNum=startNum+step) {
            angka[index]=startNum;
            index++;
            }
            result=angka;
    }else if(startNum>finishNum){
        for(startNum; startNum >= finishNum; startNum=startNum-step) {
            angka[index]=startNum;
            index++;
            }
            result=angka;
    }else if(startNum==undefined || finishNum==undefined){
        result= -1;
    }
   
    return result;
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal No. 3 (Sum of Range)
console.log(""); 
console.log("Soal No. 3 (Sum of Range)");
console.log("Jawaban : "); 

function sum(startNum,finishNum,step=1){
    var result=0;
    if(startNum!=undefined && finishNum!=undefined){
        result=rangeWithStep(startNum,finishNum,step);
        result=result.reduce((a, b) => a + b, 0)
    }else if (startNum==undefined && finishNum==undefined){
        result=0;
    }else{
        result=startNum;
    }
    return result;
}

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1));// 1
console.log(sum()); // 0 


//Soal No. 4 (Array Multidimensi))
console.log(""); 
console.log("Soal No. 4 (Array Multidimensi)");
console.log("Jawaban : "); 

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

function dataHandling (input=[]){
    for(var i=0; i<input.length;i++){     
        console.log("Nomor ID:\t"+input[i][0]);
        console.log("Nama Lengkap:\t"+input[i][1]); 
        console.log("TTL:\t"+input[i][2]+" "+input[i][3]); 
        console.log("Hobi:\t"+input[i][4]); 
        console.log("\n"); 
    }
}

dataHandling(input);

//Soal No. 5 (Balik Kata))
console.log(""); 
console.log("Soal No. 5 (Balik Kata)");
console.log("Jawaban : ");


function balikKata (string){
    var result="";
    for(var i=string.length; i>0;i--){
        result=result+string[i-1];
    }
    return result;
}


console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 


//Soal No. 6 (Metode Array))
console.log(""); 
console.log("Soal No. 6 (Metode Array)");
console.log("Jawaban : ");

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input=[]){
    input.splice(1,1,input[1]+" Elsharawy");
    input.splice(2,1,"Provinsi "+ input[2]);
    input.splice(4,1,"Pria")
    input.splice(5,0,"SMA Internasional Metro")
    console.log(input);

    var tanggal=input[3].split("/");
    switch(tanggal[1]) {
        case '01':   { console.log('Januari'); break; }
        case '02':   { console.log('Pebruari'); break; }
        case '03':   { console.log('Maret'); break; }
        case '04':   { console.log('April'); break; }
        case '05':   { console.log('Mei'); break; }
        case '06':   { console.log('Juni'); break; }
        case '07':   { console.log('Juli'); break; }
        case '08':   { console.log('Agustus'); break; }
        case '09':   { console.log('September'); break; }
        case '10':   { console.log('Oktober'); break; }
        case '11':   { console.log('November'); break; }
        case '12':   { console.log('Desember'); break; }
        default:  { break; }
    }

    tanggal.sort(function(a, b){return b-a}); 
    console.log(tanggal);
    console.log(input[3].split("/").join('-'));
    console.log(input[1].slice(0,15));

}

dataHandling2(input);



