// Tulis code untuk memanggil function readBooks di sini
//1. Mengubah fungsi menjadi fungsi arrow


const golden = function goldenFunction(){
    console.log("this is golden!!")
}
console.log("1. Mengubah fungsi menjadi fungsi arrow");  
golden()

console.log(""); 
console.log("Jawaban : ");

const golden_es6 = () => {
    console.log("this is golden!!")
} 

golden_es6();
console.log(""); 

// Tulis code untuk memanggil function readBooks di sini
//2. Sederhanakan menjadi Object literal di ES6
console.log("2. Sederhanakan menjadi Object literal di ES6");
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

console.log(""); 
console.log("Jawaban : ");

const newFunctiones6 = ((firstName, lastName) => {
    firstName
    lastName
  function fullName()  {  console.log(firstName + " " + lastName); }
   return {
    firstName,
    lastName,
    fullName
  }

});
newFunctiones6("William", "Imoh").fullName() 
console.log(""); 


// Tulis code untuk memanggil function readBooks di sini
//3. Destructuring
console.log("3. Destructuring");
console.log("Jawaban : ");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName, destination, occupation)
console.log(""); 


// Tulis code untuk memanggil function readBooks di sini
//4. Array Spreading
console.log("4. Array Spreading");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
console.log("Jawaban : ");
let combinede6s = [...west,...east]
console.log(combinede6s);
console.log(""); 



// Tulis code untuk memanggil function readBooks di sini
//5. Template Literals
console.log("5. Template Literals");
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before) 
console.log("Jawaban : ");
var afteres6 = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
veniam`;
console.log(afteres6) 