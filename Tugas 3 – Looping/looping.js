//No. 1 Looping While

console.log("No. 1 Looping While");
console.log("Jawaban : ");
var no = 2;
var no2 = 20;
while(no <= 20 || no2>=2) { 
  if (no2==20){
      if(no == 2){
        console.log("LOOPING PERTAMA");
      }
      console.log(no+" - I love coding");
      no=no+2; 
  }
  if (no==22){
    if(no2 == 20){
      console.log("LOOPING KEDUA");
    }
    console.log(no2+" - I will become a mobile developer");
    no2=no2-2; 
  }
}
console.log("");

//No. 2 Looping menggunakan

console.log("No. 2 Looping menggunakan");
console.log("Jawaban : ");
var string="";
for(var no = 1; no <= 20; no++) {
    if (no%2==0){
        string="Berkualitas"
    } else if(no%3==0){
        string="I Love Coding"
    } else {
        string="Santai"
    }
    
    console.log(no+" - "+string);
  }
  console.log("");

//No. 3 Membuat Persegi Panjang

console.log("No. 3 Membuat Persegi Panjang");
console.log("Jawaban : ");
var string="";
for(var i = 1; i <= 4; i++) {
    for(var j = 1; j <= 8; j++) {
        var string=string+"#";
    }
    console.log(string);
    string="";
  }
  console.log("");

//No. 4 Membuat Tangga

console.log("No. 4 Membuat Tangga");
console.log("Jawaban : ");
var string="";
for(var i = 1; i <= 7; i++) {
    for(var j = 1; j <= i; j++) {
        string=string+"#";
    }
    console.log(string);
    string="";
  }
  console.log("");

//No. 5 Membuat Papan Catur

console.log("No. 5 Membuat Papan Catur");
console.log("Jawaban : ");
var string=" ";

for(var i = 1; i <= 8; i++) {
    for(var j = 1; j < 8; j++) {
        if(string.substr(string.length-1)==" "){
            string=string+"#";
        }else{
            string=string+" ";
        }
    }
    console.log(string);
    string=string.substr(string.length-1);
  
  }
   
  console.log("");